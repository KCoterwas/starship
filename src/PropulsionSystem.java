public class PropulsionSystem {

	private int speed;
	
	public PropulsionSystem(){
		speed = 0;
	}
	
	public void increase(){
		System.err.println("Calling increase...");
		speed += 5;
		System.err.println("End of increase.");
	}
	
	public void decrease(){
		System.err.println("Calling decrease...");
		if(speed >= 5){
			speed -= 5;
		}
		System.err.println("End of decrease.");
	}
	
	public int read(){
		System.err.println("Calling read...");
		System.err.println("End of read.");
		return speed;
	}
	
}
