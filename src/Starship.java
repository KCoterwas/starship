import java.util.Scanner;
public class Starship {

	private PropulsionSystem propulsion;
	
	public Starship(){
		this.propulsion = new PropulsionSystem();
	}
	
	private void inputCommand(String str){
		System.err.println("Calling inputCommand...");
		str = str.trim();
		str = str.toLowerCase();
		
		if(str.equals("accel")){
			propulsion.increase();
		}else if(str.equals("decel")){
			propulsion.decrease();
		}else if(str.equals("speed")){
			System.out.println(propulsion.read());
		}else{
			System.out.println("Invalid Command");
		}
		System.err.println("End of inputCommand.");
	}
	
	public static void main (String[] args){
		System.out.print("Please enter a command: ");
		
		Starship SuperShip = new Starship();
		
		Scanner userInput = new Scanner(System.in);
		String inputStr;

		while(true){
			inputStr = userInput.nextLine();
			inputStr = inputStr.trim();
			inputStr = inputStr.toLowerCase();
			if(inputStr.equals("quit")){
				break;
			}
			else
				SuperShip.inputCommand(inputStr);
		}
		
	}
	
}
